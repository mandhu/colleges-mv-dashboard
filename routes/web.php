<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('levels', 'LevelController');

Route::resource('categories', 'CategoryController');

Route::resource('institutes', 'InstituteController');

Route::resource('courses', 'CourseController');

Route::get('courses/modules/{id}', 'CourseController@course')->name('course.module');

Route::resource('modules', 'ModuleController');

Route::resource('photos', 'PhotoController');
