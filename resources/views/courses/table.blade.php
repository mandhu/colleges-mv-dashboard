<table class="table table-responsive" id="courses-table">
    <thead>
        <tr>
            <th>Code</th>
        <th>Name</th>
        {{-- <th>Description</th> --}}
        <th>Duration</th>
        <th>Institute</th>
        <th>Category</th>
        <th>Level</th>
        <th>Mqa Level</th>
        <th>Fee</th>
        {{-- <th>Requirement</th>
            <th colspan="3">Action</th>
        </tr> --}}
    </thead>
    <tbody>
    @foreach($courses as $course)
        <tr>
            <td>{!! $course->code !!}</td>
            <td>{!! $course->name !!}</td>
            {{-- <td>{!! $course->description !!}</td> --}}
            <td>{!! $course->duration !!}</td>
            <td>{!! $course->institute->name !!}</td>
            <td>{!! $course->category->name !!}</td>
            <td>{!! $course->level->name !!}</td>
            <td>{!! $course->mqa_level !!}</td>
            <td>{!! $course->fee !!}</td>
            {{-- <td>{!! $course->requirement !!}</td> --}}
            <td>
                {!! Form::open(['route' => ['courses.destroy', $course->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('courses.show', [$course->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('courses.edit', [$course->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>