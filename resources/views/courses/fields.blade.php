<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'Code:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 3]) !!}
</div>

<!-- Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duration', 'Duration:') !!}
    {!! Form::text('duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Institute Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('institute_id', 'Select Institute:') !!}
    {!!Form::select('institute_id', $institutes->pluck('name', 'id'), null, ['class' => 'form-control'])!!}
</div>

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Select Category:') !!}
    {!!Form::select('category_id', $categories->pluck('name', 'id'), null, ['class' => 'form-control'])!!}
</div>

<!-- Level Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('level_id', 'Select Level:') !!}
    {!!Form::select('level_id', $levels->pluck('name', 'id'), null, ['class' => 'form-control'])!!}
</div>

<!-- Mqa Level Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mqa_level', 'Mqa Level:') !!}
    {!! Form::text('mqa_level', null, ['class' => 'form-control']) !!}
</div>

<!-- Fee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fee', 'Fee:') !!}
    {!! Form::text('fee', null, ['class' => 'form-control']) !!}
</div>

<!-- Requirement Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('requirement', 'Requirement:') !!}
    {!! Form::textarea('requirement', null, ['class' => 'form-control', 'rows' => 3]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('courses.index') !!}" class="btn btn-default">Cancel</a>
</div>
