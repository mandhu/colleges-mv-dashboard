@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Course
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('courses.show_fields')
                </div>
                <div class="row">
                   <div class="col-md-6">
                       <h3>Modules</h3>
                   </div>
                   <div class="col-md-6">
                        <a href="{!! route('course.module', [$course->id]) !!}" class="btn btn-default pull-right">Add Module</a>
                   </div>
                   <div class="col-md-12">
                        <ul class="list-group">
                            @forelse ($course->modules as $module)
                                <li class="list-group-item">{{ $module->name}} ({{ $module->code}})</li>
                            @empty
                                <li class="list-group-item">No module found</li>
                            @endforelse
                        </ul>
                   </div>
                    
               </div>
               <div class="row">
                   <div class="col-md-12">
                       <a href="{!! route('courses.index') !!}" class="btn btn-default">Back</a>
                   </div>
               </div>
            </div>
        </div>
    </div>
@endsection
