<!-- Id Field -->
{{-- <div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $course->id !!}</p>
</div> --}}

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $course->code !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $course->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $course->description !!}</p>
</div>

<!-- Duration Field -->
<div class="form-group">
    {!! Form::label('duration', 'Duration:') !!}
    <p>{!! $course->duration !!}</p>
</div>

<!-- Institute Id Field -->
<div class="form-group">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    <p>{!! $course->institute->name !!}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Category Id:') !!}
    <p>{!! $course->category->name !!}</p>
</div>

<!-- Level Id Field -->
<div class="form-group">
    {!! Form::label('level_id', 'Level Id:') !!}
    <p>{!! $course->level->name !!}</p>
</div>

<!-- Mqa Level Field -->
<div class="form-group">
    {!! Form::label('mqa_level', 'Mqa Level:') !!}
    <p>{!! $course->mqa_level !!}</p>
</div>

<!-- Fee Field -->
<div class="form-group">
    {!! Form::label('fee', 'Fee:') !!}
    <p>{!! $course->fee !!}</p>
</div>

<!-- Requirement Field -->
<div class="form-group">
    {!! Form::label('requirement', 'Requirement:') !!}
    <p>{!! $course->requirement !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! \Carbon\Carbon::parse($course->created_at)->format('d MM Y')!!}</p>
</div>

<!-- Updated At Field -->
{{-- <div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $course->updated_at !!}</p>
</div> --}}

