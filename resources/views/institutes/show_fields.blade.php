<!-- Id Field -->
{{-- <div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $institute->id !!}</p>
</div> --}}

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $institute->name !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $institute->phone !!}</p>
</div>

<!-- Fax Field -->
<div class="form-group">
    {!! Form::label('fax', 'Fax:') !!}
    <p>{!! $institute->fax !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $institute->email !!}</p>
</div>

<!-- Website Field -->
<div class="form-group">
    {!! Form::label('website', 'Website:') !!}
    <p>{!! $institute->website !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $institute->address !!}</p>
</div>

<!-- Established At Field -->
<div class="form-group">
    {!! Form::label('established_at', 'Established At:') !!}
    <p>{!! \Carbon\Carbon::parse($institute->established_at)->format('d MM Y') !!}</p>
</div>

<!-- Primary Color Field -->
<div class="form-group">
    {!! Form::label('primary_color', 'Primary Color:') !!}
    <p style="color:{{$institute->primary_color}}">{!! $institute->primary_color !!}</p>
</div>

<!-- Secondary Color Field -->
<div class="form-group">
    {!! Form::label('secondary_color', 'Secondary Color:') !!}
    <p style="color:{{$institute->secondary_color}}">{!! $institute->secondary_color !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! \Carbon\Carbon::parse($institute->created_at)->format('d MM Y') !!}</p>
</div>

<!-- Updated At Field -->
{{-- <div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $institute->updated_at !!}</p>
</div> --}}

