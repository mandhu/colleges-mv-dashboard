@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Institute
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($institute, ['route' => ['institutes.update', $institute->id], 'method' => 'patch']) !!}

                        @include('institutes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection