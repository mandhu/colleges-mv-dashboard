<table class="table table-responsive" id="institutes-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Phone</th>
        <th>Fax</th>
        <th>Email</th>
        <th>Website</th>
        <th>Address</th>
        <th>Established At</th>
        <th>Primary Color</th>
        <th>Secondary Color</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($institutes as $institute)
        <tr>
            <td>{!! $institute->name !!}</td>
            <td>{!! $institute->phone !!}</td>
            <td>{!! $institute->fax !!}</td>
            <td>{!! $institute->email !!}</td>
            <td>{!! $institute->website !!}</td>
            <td>{!! $institute->address !!}</td>
            <td>{!! \Carbon\Carbon::parse($institute->established_at)->format('d MM Y') !!}</td>
            <td style="color:{{$institute->primary_color}}">{!! $institute->primary_color !!}</td>
            <td style="color:{{$institute->secondary_color}}">{!! $institute->secondary_color !!}</td>
            <td>
                {!! Form::open(['route' => ['institutes.destroy', $institute->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('institutes.show', [$institute->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('institutes.edit', [$institute->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>