<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Fax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fax', 'Fax:') !!}
    {!! Form::text('fax', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Website Field -->
<div class="form-group col-sm-6">
    {!! Form::label('website', 'Website:') !!}
    {!! Form::text('website', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Established At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('established_at', 'Established At:') !!}
    {!! Form::date('established_at', $institute ?  $institute->established_at : null, ['class' => 'form-control']) !!}
</div>

<!-- Primary Color Field -->
<div class="form-group col-sm-3">
    {!! Form::label('primary_color', 'Primary Color:') !!}
    {!! Form::color('primary_color', null, ['class' => 'form-control w-45']) !!}
</div>

<!-- Secondary Color Field -->
<div class="form-group col-sm-3">
    {!! Form::label('secondary_color', 'Secondary Color:') !!}
    {!! Form::color('secondary_color', null, ['class' => 'form-control w-45']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('institutes.index') !!}" class="btn btn-default">Cancel</a>
</div>
