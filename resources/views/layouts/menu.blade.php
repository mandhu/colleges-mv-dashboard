

<li class="{{ Request::is('levels*') ? 'active' : '' }}">
    <a href="{!! route('levels.index') !!}"><i class="fa fa-edit"></i><span>Levels</span></a>
</li>

<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('categories.index') !!}"><i class="fa fa-edit"></i><span>Categories</span></a>
</li>

<li class="{{ Request::is('institutes*') ? 'active' : '' }}">
    <a href="{!! route('institutes.index') !!}"><i class="fa fa-edit"></i><span>Institutes</span></a>
</li>

<li class="{{ Request::is('courses*') ? 'active' : '' }}">
    <a href="{!! route('courses.index') !!}"><i class="fa fa-edit"></i><span>Courses</span></a>
</li>
{{-- 
<li class="{{ Request::is('modules*') ? 'active' : '' }}">
    <a href="{!! route('modules.index') !!}"><i class="fa fa-edit"></i><span>Modules</span></a>
</li>

<li class="{{ Request::is('photos*') ? 'active' : '' }}">
    <a href="{!! route('photos.index') !!}"><i class="fa fa-edit"></i><span>Photos</span></a>
</li> --}}

{{-- <li class="{{ Request::is('tests*') ? 'active' : '' }}">
    <a href="{!! route('tests.index') !!}"><i class="fa fa-edit"></i><span>Tests</span></a>
</li> --}}

