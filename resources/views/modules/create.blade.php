@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            {{ $course->name}}
        </h1>
        <div class="clearfix"></div>

            @include('flash::message')

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
                <h3>Modules</h3>
                <ul class="list-group">
                    @forelse ($course->modules as $module)
                        <li class="list-group-item">{{ $module->name}} ({{ $module->name}})</li>
                    @empty
                        <li class="list-group-item">No module found</li>
                    @endforelse
                </ul>
            </div>
        </div>
    </section>
    
    
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'modules.store']) !!}

                        @include('modules.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
