<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateInstituteAPIRequest;
use App\Http\Requests\API\UpdateInstituteAPIRequest;
use App\Models\Institute;
use App\Repositories\InstituteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class InstituteController
 * @package App\Http\Controllers\API
 */

class InstituteAPIController extends AppBaseController
{
    /** @var  InstituteRepository */
    private $instituteRepository;

    public function __construct(InstituteRepository $instituteRepo)
    {
        $this->instituteRepository = $instituteRepo;
    }

    /**
     * Display a listing of the Institute.
     * GET|HEAD /institutes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->instituteRepository->pushCriteria(new RequestCriteria($request));
        $this->instituteRepository->pushCriteria(new LimitOffsetCriteria($request));
        $institutes = $this->instituteRepository->with([
            'courses' => function($query) {
                $query->with(['level', 'category', 'modules', 'institute']);
            }])->all();

        return $this->sendResponse($institutes->toArray(), 'Institutes retrieved successfully');
    }

    /**
     * Store a newly created Institute in storage.
     * POST /institutes
     *
     * @param CreateInstituteAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInstituteAPIRequest $request)
    {
        $input = $request->all();

        $institutes = $this->instituteRepository->create($input);

        return $this->sendResponse($institutes->toArray(), 'Institute saved successfully');
    }

    /**
     * Display the specified Institute.
     * GET|HEAD /institutes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Institute $institute */
        $institute = $this->instituteRepository->findWithoutFail($id);

        if (empty($institute)) {
            return $this->sendError('Institute not found');
        }

        return $this->sendResponse($institute->toArray(), 'Institute retrieved successfully');
    }

    /**
     * Update the specified Institute in storage.
     * PUT/PATCH /institutes/{id}
     *
     * @param  int $id
     * @param UpdateInstituteAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInstituteAPIRequest $request)
    {
        $input = $request->all();

        /** @var Institute $institute */
        $institute = $this->instituteRepository->findWithoutFail($id);

        if (empty($institute)) {
            return $this->sendError('Institute not found');
        }

        $institute = $this->instituteRepository->update($input, $id);

        return $this->sendResponse($institute->toArray(), 'Institute updated successfully');
    }

    /**
     * Remove the specified Institute from storage.
     * DELETE /institutes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Institute $institute */
        $institute = $this->instituteRepository->findWithoutFail($id);

        if (empty($institute)) {
            return $this->sendError('Institute not found');
        }

        $institute->delete();

        return $this->sendResponse($id, 'Institute deleted successfully');
    }
}
