<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInstituteRequest;
use App\Http\Requests\UpdateInstituteRequest;
use App\Repositories\InstituteRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class InstituteController extends AppBaseController
{
    /** @var  InstituteRepository */
    private $instituteRepository;

    public function __construct(InstituteRepository $instituteRepo)
    {
        $this->middleware('auth');
        $this->instituteRepository = $instituteRepo;
    }

    /**
     * Display a listing of the Institute.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->instituteRepository->pushCriteria(new RequestCriteria($request));
        $institutes = $this->instituteRepository->all();

        return view('institutes.index')
            ->with('institutes', $institutes);
    }

    /**
     * Show the form for creating a new Institute.
     *
     * @return Response
     */
    public function create()
    {
        $institute = null;
        return view('institutes.create')->with('institute', $institute);
    }

    /**
     * Store a newly created Institute in storage.
     *
     * @param CreateInstituteRequest $request
     *
     * @return Response
     */
    public function store(CreateInstituteRequest $request)
    {
        $input = $request->all();

        $institute = $this->instituteRepository->create($input);

        Flash::success('Institute saved successfully.');

        return redirect(route('institutes.index'));
    }

    /**
     * Display the specified Institute.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $institute = $this->instituteRepository->findWithoutFail($id);

        if (empty($institute)) {
            Flash::error('Institute not found');

            return redirect(route('institutes.index'));
        }

        return view('institutes.show')->with('institute', $institute);
    }

    /**
     * Show the form for editing the specified Institute.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $institute = $this->instituteRepository->findWithoutFail($id);

        if (empty($institute)) {
            Flash::error('Institute not found');

            return redirect(route('institutes.index'));
        }

        return view('institutes.edit')->with('institute', $institute);
    }

    /**
     * Update the specified Institute in storage.
     *
     * @param  int              $id
     * @param UpdateInstituteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInstituteRequest $request)
    {
        $institute = $this->instituteRepository->findWithoutFail($id);

        if (empty($institute)) {
            Flash::error('Institute not found');

            return redirect(route('institutes.index'));
        }

        $institute = $this->instituteRepository->update($request->all(), $id);

        Flash::success('Institute updated successfully.');

        return redirect(route('institutes.index'));
    }

    /**
     * Remove the specified Institute from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $institute = $this->instituteRepository->findWithoutFail($id);

        if (empty($institute)) {
            Flash::error('Institute not found');

            return redirect(route('institutes.index'));
        }

        $this->instituteRepository->delete($id);

        Flash::success('Institute deleted successfully.');

        return redirect(route('institutes.index'));
    }
}
