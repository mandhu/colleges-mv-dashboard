<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Institute
 * @package App\Models
 * @version October 8, 2018, 1:02 pm UTC
 *
 * @property string name
 * @property string phone
 * @property string fax
 * @property string email
 * @property string website
 * @property string address
 * @property date established_at
 * @property string primary_color
 * @property string secondary_color
 */
class Institute extends Model
{
    use SoftDeletes;

    public $table = 'institutes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'phone',
        'fax',
        'email',
        'website',
        'address',
        'established_at',
        'primary_color',
        'secondary_color'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'phone' => 'string',
        'fax' => 'string',
        'email' => 'string',
        'website' => 'string',
        'address' => 'string',
        'established_at' => 'date',
        'primary_color' => 'string',
        'secondary_color' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'phone' => 'required',
        'fax' => 'required',
        'email' => 'required',
        'website' => 'required',
        'address' => 'required',
        'established_at' => 'required',
        'primary_color' => 'required',
        'secondary_color' => 'required'
    ];

    public function courses () {
        return $this->hasMany(Course::class);
    }

    
}
