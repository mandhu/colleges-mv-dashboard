<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Course
 * @package App\Models
 * @version October 8, 2018, 1:09 pm UTC
 *
 * @property string code
 * @property string name
 * @property string description
 * @property string duration
 * @property integer institute_id
 * @property integer category_id
 * @property integer level_id
 * @property integer mqa_level
 * @property string fee
 * @property string requirement
 */
class Course extends Model
{
    use SoftDeletes;

    public $table = 'courses';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'code',
        'name',
        'description',
        'duration',
        'institute_id',
        'category_id',
        'level_id',
        'mqa_level',
        'fee',
        'requirement'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'code' => 'string',
        'name' => 'string',
        'description' => 'string',
        'duration' => 'string',
        'institute_id' => 'integer',
        'category_id' => 'integer',
        'level_id' => 'integer',
        'mqa_level' => 'integer',
        'fee' => 'string',
        'requirement' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'code' => 'required',
        'name' => 'required',
        'description' => 'required',
        'duration' => 'required',
        'institute_id' => 'required',
        'category_id' => 'required',
        'level_id' => 'required',
        'mqa_level' => 'required',
        'fee' => 'required',
        'requirement' => 'required'
    ];

    public function institute() {
        return $this->belongsTo(Institute::class);
    }

    public function level() {
        return $this->belongsTo(Level::class);
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function modules() {
        return $this->hasMany(Module::class);
    }

    
}
