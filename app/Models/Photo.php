<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Photo
 * @package App\Models
 * @version October 8, 2018, 1:13 pm UTC
 *
 * @property integer institute_id
 * @property string type
 * @property string file_name
 */
class Photo extends Model
{
    use SoftDeletes;

    public $table = 'photos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'institute_id',
        'type',
        'file_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'institute_id' => 'integer',
        'type' => 'string',
        'file_name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'institute_id' => 'required',
        'type' => 'required',
        'file_name' => 'required'
    ];

    
}
