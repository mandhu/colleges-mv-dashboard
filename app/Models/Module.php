<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Module
 * @package App\Models
 * @version October 8, 2018, 1:11 pm UTC
 *
 * @property string code
 * @property string name
 * @property string description
 */
class Module extends Model
{
    use SoftDeletes;

    public $table = 'modules';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'course_id',
        'code',
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'code' => 'string',
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'code' => 'required',
        'course_id' => 'required',
        'name' => 'required'
    ];

    
}
