<?php

namespace App\Repositories;

use App\Models\Institute;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class InstituteRepository
 * @package App\Repositories
 * @version October 8, 2018, 1:02 pm UTC
 *
 * @method Institute findWithoutFail($id, $columns = ['*'])
 * @method Institute find($id, $columns = ['*'])
 * @method Institute first($columns = ['*'])
*/
class InstituteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name' => 'like',
        'phone',
        'fax',
        'email',
        'website',
        'address',
        'established_at',
        'primary_color',
        'secondary_color'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Institute::class;
    }
}
